module sftreg #(parameter N = 16) 
			(output logic ecrpdata, 
    		input logic data, clk, n_reset);
    		
	logic srin, srin1, srin2, srin3, srout;
	logic [15:0] q ;

always_comb 

begin

	srout = q[0]; 
	
	// shift register out as bit 0
	
	srin = (q[0] ^ q[2] ^ q[3] ^ q[5]); 
	
	// xor gates
	 
	srin1 = (q[0] | q[1] |  q[2] | q[3] |  q[4] | q[5] |  
		      q[6] | q[7] |  q[8] | q[9] |  q[10] | q[11] |
		      q[12] | q[13] |  q[14] |  q[15]);
		      
	srin3 = !(srin1);
	
	//nor gate
	
	srin2 = srin3 | srin;
		
	// Shift register in logic
	
	ecrpdata = data ^ srout;	
	
	// xor the shift register out with the data in.
		  
end

always_ff @(negedge clk, negedge n_reset)
	
  if (!n_reset)
    q <= '0; // resets the shift registers bits to 0 
  else
    begin
      q <= { srin2 , q [N-1:1]}; // shift bits, and adds the new bit
    end
    
// shift register 16bit

endmodule 
