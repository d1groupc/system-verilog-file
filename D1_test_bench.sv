module testbaggette;
	logic ecrpdata, data;
	logic clk, n_reset; 	// defining logic inputs and outputs.
	
	sftreg s (.*); 			// create sftreg instance.
	
initial
begin
	n_reset = 1;
	#5ns
	n_reset = 0;
	#5ns
	n_reset = 1;			// initial press of the reset button.
end 

					
always 
begin

	#200ns					// delay, so there is time for initial reset.
	clk = 1;
	data = 1;
	#5ns;
	clk = 0;
	#25ns clk = 1;
	#20ns data = 0;			// 50% duty cycle.
	#5ns;					// time between data changing and clock falling edge.
	clk = 0;
	#25ns clk = 1;
	#20ns data = 0;
	#5ns;
	clk = 0;
	#25ns clk = 1;
	#20ns data = 1;
	#5ns;
	clk = 0;
	#25ns clk = 1;
	#20ns data = 0;
	#5ns;
	clk = 0;
	#25ns clk = 1;
	#20ns data = 1;
	#5ns;
	clk = 0;
	#25ns clk = 1;
	#20ns data = 0;
	#5ns;
	clk = 0;
	#25ns clk = 1;
	#20ns data = 0;
	#5ns;
	clk = 0;
	#25ns clk = 1;
	#20ns data = 1;
	#5ns;
	clk = 0;
	#25ns clk = 1;
	#20ns data = 0;
	#5ns;
	clk = 0;
	#25ns clk = 1;
	#20ns data = 1;
	#5ns;
	clk = 0;
	
	
// one input data bit 'b 10010100101

end
endmodule